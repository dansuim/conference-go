from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_city_photo(city, state):
    # Create the URL for the request with the city and state
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Make the request
    response = requests.get(url, headers=headers)
    # Parse the JSON response
    data = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response

    # picture_url = data["photos"][0]["src"]["original"]
    # return picture_url
    return data["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    urlgeocode = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response = requests.get(urlgeocode)
    # Parse the JSON response
    data = json.loads(response.content)
    print(data)
    # Get the latitude and longitude from the response
    # geocode = {"lat": data["coord"]}
    geocodelat = int(data["coord"]["lat"])
    geocodelon = int(data["coord"]["lon"])

    # Create the URL for the current weather API with the latitude
    #   and longitude
    urlweather = f"https://api.openweathermap.org/data/2.5/weather?lat={geocodelat}&lon={geocodelon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    # Make the request
    weatherresponse = requests.get(urlweather)
    # Parse the JSON response
    weatherdata = json.loads(weatherresponse.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    weather = {
        "temp": weatherdata["main"]["temp"],
        "description": weatherdata["weather"]["description"],
    }
    # Return the dictionary
    return weather
